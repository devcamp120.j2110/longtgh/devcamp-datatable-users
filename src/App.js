import Datatable from "./components/Datatable";
import './App.css'

function App() {
  return (
    <div>
      <Datatable />
    </div>
  );
}

export default App;
