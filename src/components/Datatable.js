import { Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, styled, Button} from "@mui/material";

import {useState, useEffect} from 'react';

import DeleteModal from "./Modal/DeleteModal";
import InsertModal from "./Modal/InsertModal";
import UpdateModal from "./Modal/UpdateModal";

function Datatable() {
    const Item = styled(Paper)(({ theme }) => ({
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }));    

    const [users, setUsers] = useState([]);
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0)
    const [limit, setLimit] = useState(10);
    const [currentUser, setCurrentUser] = useState({
        id: null,
        firstname: "",
        lastname: "",
        customerType: "",
        registerStatus: "",
        country: "",
        subject: ""
    });

    const [insertModal, setInsertModal] = useState(false);
    const [updateModal, setUpdateModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    

    const getData = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-register-java-api/users");
        const data = await response.json();
        return data;
    }

    const changeHandler = (event, value) => {
        setPage(value);
    }
    
    const changeSelectHandler = e => {
        setLimit(e.target.value);
    }

    const onBtnAddClick = () => {
        console.log("Click nút thêm!");
        setInsertModal(true);
    }    

    const onBtnEditClick = (value, event) => {
        console.log("Click nút sửa, UserId= " + value.id);
        setCurrentUser(value);
        setUpdateModal(true);            
    }

    const onBtnDeleteClick = (data) => {
        console.log("Click nút xóa, UserId= " + data.id);
        setCurrentUser(data);
        setDeleteModal(true);
    }

    useEffect(() => {        
        getData()
            .then((data) => {
                console.log(data);                      
                setUsers(data.slice((page - 1) * limit, page * limit));
                setNoPage(Math.ceil(data.length / limit));
            })
            .catch((error) => {
                console.log(error);
            })
    }, [page, limit])


    return (
        <>
        <Container>
           <Grid container marginTop={10} rowSpacing={1}>
                <Grid item xs={12} md={12} sm={12} lg={12} marginBottom={2}>
                    <Item><h2>DANH SÁCH ĐĂNG KÝ</h2></Item>
                </Grid>
                <Button variant="contained" color="success" onClick={onBtnAddClick}>Thêm</Button>
                <Grid item xs={12} md={12} sm={12} lg={12}>
                    <label>Chọn số lượng user hiển thị &nbsp;</label>
                    <select onChange={changeSelectHandler}>
                        <option value={5}>5</option>
                        <option value={10} defaultValue>10</option>
                        <option value={25}>25</option>
                        <option value={50}>50</option>
                    </select>
                </Grid>
                <Grid item xs={12} md={12} sm={12} lg={12}>                    
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="users table">
                            <TableHead className="table-header">
                                <TableRow>
                                    <TableCell >Mã người dùng</TableCell>
                                    <TableCell >Firstname</TableCell>
                                    <TableCell >Lastname</TableCell>
                                    <TableCell >Country</TableCell>
                                    <TableCell >Subject</TableCell>
                                    <TableCell >Customer Type</TableCell>
                                    <TableCell >Register Status</TableCell>
                                    <TableCell >Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {users.map((element, index) => (
                                    <TableRow 
                                        key={index} 
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}                                        
                                    >
                                        <TableCell component="th" scope="row">{element.id}</TableCell>
                                        <TableCell>{element.firstname}</TableCell>
                                        <TableCell>{element.lastname}</TableCell>
                                        <TableCell>{element.country}</TableCell>
                                        <TableCell>{element.subject}</TableCell>
                                        <TableCell>{element.customerType}</TableCell>
                                        <TableCell>{element.registerStatus}</TableCell>
                                        <TableCell>
                                            <Button onClick={onBtnEditClick.bind(this,element)} variant='contained'>Sửa</Button>&nbsp;
                                            <Button onClick={()=> onBtnDeleteClick(element)} variant="contained" color="error">Xóa</Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                <Grid item xs={12} md={12} sm={12} lg={12} marginTop={5} marginBottom={5}>
                    <Pagination onChange={changeHandler} count={noPage} defaultPage={page}></Pagination>
                </Grid>
           </Grid>
           
        </Container>
                
        <UpdateModal update={updateModal} setUpdate={setUpdateModal} user={currentUser}/>                       
        <InsertModal insert={insertModal} setInsert={setInsertModal}/>
        <DeleteModal deleteModal={deleteModal} setDelete={setDeleteModal} user={currentUser}/>
        </>       
    )
}

export default Datatable;
