import { Modal, Box, Grid, Typography, Button} from "@mui/material";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


function DeleteModal({deleteModal, setDelete, user}){
    const handleClose = () => setDelete(false);
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }

    const onBtnConfirmClick = () =>{
        console.log("Click xác nhận xóa!");
        let body = { method: 'DELETE'};
        let vId = user.id;
        fetchApi("http://42.115.221.44:8080/devcamp-register-java-api/users/" + vId, body)
        .then((data)=>{
            console.log(data);
            toast("Bạn đã xóa thành công user mang id= " + vId);
            handleClose();
        })
        .catch((err) => {
            console.log(err);
            toast("Lỗi rồi!");
        })
    }
    
    return(
        <>
        <Modal      
            open={deleteModal}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Confirm Xóa User {user.firstname}
                </Typography>
                <Grid mt={5}>
                    <Button variant="contained" color="error" onClick={onBtnConfirmClick}>Xác nhận</Button>
                    <Button variant="contained" color="success" onClick={handleClose} style={{float:"right"}}>Hủy bỏ</Button>
                </Grid>
            </Box>                
        </Modal>
        <ToastContainer/>
        </>
    )
}

export default DeleteModal;