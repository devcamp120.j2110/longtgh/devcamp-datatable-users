import { Modal, Box, Grid, TextField, Typography, Button} from "@mui/material";
import { ToastContainer, toast } from 'react-toastify';
import {useState, useEffect} from 'react';
import 'react-toastify/dist/ReactToastify.css';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function InsertModal({insert, setInsert}) {
    const [fname, setFName] = useState("");
    const [lname, setLName] = useState("");
    const [subject, setSubject] = useState("");
    const [country, setCountry] = useState("");   
    const [status, setStatus] = useState("");    
    const [type, setType] = useState("");

    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }

    const handleClose = () => setInsert(false);
    const onBtnInsertClick = () => {
        console.log("Click nút Insert!");
        // tạo user obj 
        let vNewUser = {
            firstname: fname.trim(),
            lastname: lname.trim(),            
            country: country,
            subject: subject.trim(),
            customerType: type,
            registerStatus: status
        }

        // kiểm tra thông tin nhập vào
        if (!vNewUser.firstname || !vNewUser.lastname || !vNewUser.subject) {
            alert("Bạn cần phải điền đầy đủ thông tin !");
            return false;
        }
        else {
            console.log("Đã validate data xong!");
            let body = {
                method: 'POST',
                body: JSON.stringify(vNewUser) ,
                headers: {
                  'Content-type': 'application/json; charset=UTF-8',
                },
            }
            fetchApi("http://42.115.221.44:8080/devcamp-register-java-api/users", body)
            .then((data)=>{
                console.log(data);
                toast("Bạn đã tạo user thành công !");
                handleClose();
            })
            .catch((err) => {
                console.log(err);
                toast("Lỗi rồi!");
            })
        }         
    }

    const handleFirstName = (e) => setFName(e.target.value);    
    const handleLastName = (e) => setLName(e.target.value);
    const handleSubject = (e) => setSubject(e.target.value);
    const handleSelectCountry = (e) => setCountry(e.target.value);  
    const handleSelectStatus = (e) => setStatus(e.target.value);    
    const handleSelectType = (e) => setType(e.target.value);  

    useEffect(()=>{
        console.log(country);
        console.log(status);
        console.log(type);
    },[country, status, type])

    return(
        <>
        <Modal
            open={insert}
            onClose={handleClose}
            aria-labelledby="modal-detail-title"
            aria-describedby="modal-detail-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Thêm User
                </Typography>
                <Grid>
                    <TextField variant="outlined" label="First name" fullWidth onChange={handleFirstName}></TextField>
                </Grid>
                <Grid mt={5}>
                    <TextField variant="outlined" label="Last name" fullWidth onChange={handleLastName}></TextField>
                </Grid>
                <Grid mt={5}>
                    <TextField variant="outlined" label="Subject" fullWidth onChange={handleSubject}></TextField>
                </Grid>
                <Grid mt={5}>
                    <label htmlFor="country">Country</label> <br/>                  
                    <select name="country" style={{width: "100%", height: "40px", borderRadius: "5px"}} onChange={handleSelectCountry}>
                        <option value="VN">Việt Nam</option>
                        <option value="AUS">Australia</option>
                        <option value="USA">USA</option>
                        <option value="CAN">Canada</option>
                    </select>
                </Grid>
                <Grid mt={5}>
                    <label htmlFor="type">Customer Type</label> <br/>                   
                    <select name="type" style={{width: "100%", height: "40px", borderRadius: "5px"}} onChange={handleSelectType}>
                        <option value="Standard">Standard</option>    
                        <option value="Gold">Gold</option>
                        <option value="Premium">Premium</option>                                            
                    </select>
                </Grid>
                <Grid mt={5}>
                    <label htmlFor="registerStatus">Register Status</label> <br/>                   
                    <select name="registerStatus" style={{width: "100%", height: "40px", borderRadius: "5px"}} onChange={handleSelectStatus}>
                        <option value="Standard">Standard</option>    
                        <option value="Accepted">Accepted</option>
                        <option value="Denied">Denied</option>                                            
                    </select>
                </Grid>                
                <Grid mt={5} columnSpacing={4}>
                    <Button variant="contained" color="success" onClick={onBtnInsertClick}>Insert User</Button>
                    <Button variant="contained" color="success" onClick={handleClose} style={{float:"right"}}>Hủy bỏ</Button>
                </Grid>
            </Box>
        </Modal>
        <ToastContainer/>
        </>
    )
}

export default InsertModal;